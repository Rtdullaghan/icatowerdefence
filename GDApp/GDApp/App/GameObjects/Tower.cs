﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System;

namespace GDApp.App.GameObjects
{
    public class Tower
    {

        private PrimitiveObject model;
        private  Vector3 position;
        private int attack;
        private int range;

        public PrimitiveObject Model
        {
            get { return model; }
            set { model = value; }
        }
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }
        public int Attack
        {
            get { return attack; }
            set { attack = value; }
        }
        public int Range
        {
            get { return range; }
            set { range = value; }
        }
        public Tower(PrimitiveObject model, Vector3 position, int attack, int range)
        {
            this.model = model;
            this.position = position;
            this.attack = attack;
            this.range = range;
        }

        public bool CheckRange(Vector3 position)
        {
            float distance = (float)Math.Sqrt((position.X - this.position.X) + (position.Z - this.position.Z));
            if (distance > this.range)
            { return false; }
            return true;
        }

       
    }
}
