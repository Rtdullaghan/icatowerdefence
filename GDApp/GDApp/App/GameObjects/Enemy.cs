﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System;

namespace GDApp.App.GameObjects
{
    public class Enemy
    {

        private PrimitiveObject model;
        private Vector3 position;
        private int speed;
        private int health;

        public PrimitiveObject Model
        {
            get { return model; }
            set { model = value; }
        }
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }
        public int Health
        {
            get { return health; }
            set { health = value; }
        }
        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }
        Enemy()
        { }
        public Enemy(  Vector3 position, int speed, int health)
        {
           
            this.position = position;
            this.health = health;
            this.speed = speed;
        }

        public void DecreaseHealth(int damage)
        {
            this.health -= damage;
        }

        public void IncrementSpeed()
        {
            this.speed++;
        }
    }
}
